#! /bin/bash

set -e -x
rc=0

for a in gold.*.log; do
    for b in gold.*.log; do
        if [[ $a == "$b" ]]; then
            continue
        fi
        xa=${a#gold.}
        xa=${xa%.log}
        xb=${b#gold.}
        xb=${xb%.log}
        set +e
        ./main.py diff "$a" "$b" &> "goldiff.$xa.$xb.log"
        rc=$(( rc || $? ))
        set -e
    done
done

# Smaller diffs are better:
ls -larS goldiff*

exit $rc
