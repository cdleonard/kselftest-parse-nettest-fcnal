Tool to compare logs from different runs on fcnal-test.sh.

Usage:
------
::

    python3 -m pip install -e .
    ./main.py diff gold.20211110.075818.log gold.20211110.021649.log

Ideally all differences should be explain but that is difficult. Some "known
good" logs from multiple identical runs are stored inside the repository itself.
