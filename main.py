#! /usr/bin/env python3

"""Parse results of fcnal-test.sh"""

import argparse
import difflib
import logging
import re
import string
import sys
import typing
from dataclasses import dataclass, field

import junit_xml

logger = logging.getLogger(__name__)


def scrub_ping_dots(arg):
    return arg.replace(".\x08", "").replace(" \x08", "")


def scrub_control_chars(arg: str):
    return "".join(filter(lambda x: x in string.printable, arg))


def scrub_ifindex(arg):
    RE_INDEX_MATCH = re.compile(r"(index matches: expected) ([0-9]) have ([0-9])")
    if m := RE_INDEX_MATCH.search(arg):
        if m[2] == m[3]:
            arg = RE_INDEX_MATCH.sub(r"\1 X have X", arg)
        else:
            logger.warning("m[2]=%r m[3]=%r", m[2], m[3])
    arg = re.sub(r"(pktinfo: ifindex) [0-9]", r"\1 Y", arg)
    arg = re.sub(r"(bound to device (red|eth1)/)([0-9])", r"\1Z", arg)
    return arg


def scrub(arg: str) -> str:
    arg = scrub_ping_dots(arg)
    arg = scrub_control_chars(arg)
    if re.match(r"(\.+)\n", arg, re.M):
        arg = ".....\n"
    if re.match(r"(\.+)(E+)\n", arg, re.M):
        arg = ".....EEEEE\n"
    arg = scrub_ifindex(arg)
    arg = re.sub(r"(icmp_seq=1 ttl=64) time=0.([0-9]{3}) ms", r"\1 time=0.xxx ms", arg)
    arg = re.sub(
        r"(rtt min/avg/max/mdev =) 0.([0-9]{3})/0.([0-9]{3})/0.([0-9]{3})/0.000 ms",
        r"\1 0.xxx/0.yyy/0.zzz/0.000",
        arg,
    )
    arg = re.sub(r"([0-9.]+):[0-9]{5}", r"\1:xxxxx", arg)
    arg = re.sub(r"(\[[0-9a-f:]+\]):[0-9]{5}", r"\1:xxxxx", arg)
    arg = re.sub(r"fe80::[0-9a-f:]+([ %\]\n])", r"fe80::xxxx:xxxx:xxxx:xxxx\1", arg)
    return arg


@dataclass
class TestCase:
    name: str = None
    status = None
    log_lines: typing.List[str] = field(default_factory=list)

    RE_CLIENT_LINE = re.compile("^([0-9:.]+) client:(.*)", re.S | re.M)
    RE_SERVER_LINE = re.compile("^([0-9:.]+) server:(.*)", re.S | re.M)

    def get_log_text(self):
        return "".join(self.log_lines)

    def get_clean_log_lines(self):
        result = []
        client_log = []
        server_log = []
        tcp_vrf_del = (
            "TCP passive socket" in self.name
            and "ip link del red" in self.get_log_text()
        )
        for line in self.log_lines:
            line = scrub(line)
            # Ignore empty lines
            if not line.strip():
                continue
            # HACK: client closed connection message can be printed by either side.
            if tcp_vrf_del and "client closed connection" in line:
                continue
            # HACK: skip unpredictable server timeout messages on this specific test.
            if (
                self.name
                == "Global server, device client via IP_UNICAST_IF, local connection - ns-A loopback IP"
                and "server:Timed out waiting for response" in line
            ):
                continue
            if m := self.RE_CLIENT_LINE.match(line):
                client_log.append(m.group(2))
            elif m := self.RE_SERVER_LINE.match(line):
                server_log.append(m.group(2))
            else:
                result.append(line)
        # HACK: Some MD5 tests run multiple servers with unpredictable output order
        if "MD5: VRF: " in self.name:
            server_log = sorted(server_log)
        result += ("client:" + item for item in client_log)
        result += ("server:" + item for item in server_log)
        return result


def is_log_start_line(line):
    return line == "#######################################################\n"


@dataclass
class TestSuite:
    result_list: typing.List[TestCase] = field(default_factory=list)
    result_dict: typing.Dict[str, TestCase] = field(default_factory=dict)

    def iter_names(self) -> typing.Iterable[str]:
        for item in self.result_list:
            yield item.name

    def iter_result(self) -> typing.Iterable[TestCase]:
        return self.result_list

    def add(self, test_case: TestCase):
        self.result_list.append(test_case)
        self.result_dict[test_case.name] = test_case

    def read_file(self, file_name: str) -> None:
        last_item = None
        with open(file_name, "r") as f:
            log = []
            for line in f:
                log.append(line)
                # Some tests call log_test once at the end.
                #
                # Others leave processes running while reporting results
                # and those are only killed at the end of log_test and thus
                # their relevant output is shown after log_test.
                #
                # Deal with this by assigning any text between "log_test"
                # and "log_start" to the previous test.
                if is_log_start_line(line):
                    if last_item:
                        last_item.log_lines += log
                    log = []
                if line.startswith("TEST:"):
                    name = line[6:].rsplit("[", 1)[0].strip()
                    if line.endswith("[ OK ]\n"):
                        result = True
                    elif line.endswith("[FAIL]\n"):
                        result = False
                    else:
                        result = None
                        logger.warning("Failed to parse result of %r", line)
                    item = TestCase(result)
                    item.name = name
                    item.log_lines = log
                    log = []
                    self.add(item)
                    last_item = item

    def summarize(self) -> str:
        pass_count = fail_count = unknown_count = 0
        for item in self.iter_result():
            if item.status is True:
                pass_count += 1
            if item.status is False:
                fail_count += 1
            if item.status is None:
                unknown_count += 1
        return f"{pass_count} PASS {fail_count} FAIL {unknown_count} unknown"

    def to_junit_xml(this) -> junit_xml.TestSuite:
        that = junit_xml.TestSuite("fcnal-test")
        for item in this.iter_result():
            that.test_cases.append(
                junit_xml.TestCase(
                    item.name, status=item.status, stdout="".join(item.log_lines)
                )
            )
        return that


class DiffTool:
    show_raw_diff = False
    show_clean_diff = True
    diff_test_case_count = 0

    def diff_test_case(self, old: TestCase, new: TestCase) -> bool:
        name = old.name
        if old.status != new.status:
            logger.info("DIFF status on %r: %r -> %r", name, old.status, new.status)
            return True
        old_log = old.get_clean_log_lines()
        new_log = new.get_clean_log_lines()
        if old_log != new_log:
            if self.show_raw_diff:
                old_raw_lines = list(scrub_ping_dots(x) for x in old.log_lines)
                new_raw_lines = list(scrub_ping_dots(x) for x in new.log_lines)
                logger.info(
                    "\nNDIFF RAW SHOW %r:\n%sNDIFF RAW DONE",
                    name,
                    "".join(difflib.ndiff(old_raw_lines, new_raw_lines)),
                )
            if self.show_clean_diff:
                logger.info(
                    "\nNDIFF CLEAN SHOW %r:\n%s\nNDIFF CLEAN DONE",
                    name,
                    "".join(difflib.ndiff(old_log, new_log)),
                )
            return True
        return False

    def diff_suite(self, old: TestSuite, new: TestSuite) -> bool:
        result = False
        old_names = set(old.iter_names())
        new_names = set(new.iter_names())
        old_only_names = old_names - new_names
        new_only_names = new_names - old_names
        if old_only_names:
            logger.info(
                "%d only in old: %s", len(old_only_names), sorted(old_only_names)
            )
            result |= True
        if new_only_names:
            logger.info(
                "%d only in old: %s", len(new_only_names), sorted(new_only_names)
            )
            result |= True
        common_names = old_names & new_names
        for name in sorted(common_names):
            if self.diff_test_case(old.result_dict[name], new.result_dict[name]):
                self.diff_test_case_count += 1
                result = True
        if self.diff_test_case_count:
            logger.info("%d diff cases", self.diff_test_case_count)
        return result


def create_parser():
    parser = argparse.ArgumentParser(description=__doc__)
    sp = parser.add_subparsers(dest="subcmd")

    subparser = sp.add_parser("parse", help="parse one log file")
    subparser.add_argument("file", help="Path for input")
    subparser.add_argument("--junitxml", help="Path for junitxml output")

    subparser = sp.add_parser("diff", help="compare two log files")
    subparser.add_argument("old", help="Old log file")
    subparser.add_argument("new", help="Old log file")
    subparser.add_argument(
        "--show-raw-diff",
        action="store_true",
        help="Show raw diff between per-case log",
    )

    return parser


class Tool:
    def main(self, argv=None):
        self.opts = opts = create_parser().parse_args(argv)
        if opts.subcmd == "parse":
            if not opts.file:
                raise Exception("No file specified")
            suite = TestSuite()
            suite.read_file(opts.file)
            sys.stdout.write(f"{suite.summarize()}\n")
            if opts.junitxml:
                with open(opts.junitxml, "w") as fd:
                    junitxml_suite = suite.to_junit_xml()
                    junit_xml.to_xml_report_file(fd, [junitxml_suite])
        elif opts.subcmd == "diff":
            suite_old = TestSuite()
            suite_old.read_file(opts.old)
            suite_new = TestSuite()
            suite_new.read_file(opts.new)
            logger.info("OLD: %s", opts.old)
            logger.info("NEW: %s", opts.new)
            difftool = DiffTool()
            difftool.show_raw_diff = opts.show_raw_diff
            result = difftool.diff_suite(suite_old, suite_new)
            sys.exit(result != False)
        else:
            raise Exception(f"Unknown subcommand {opts.subcmd!r}")


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    Tool().main()
