import pytest

from main import TestCase, scrub, scrub_control_chars


def test_parse_client_log():
    tc = TestCase(
        log_lines=[
            "21:08:59.292 server:server running in background\n",
            "21:08:59.300 client:Sent message:\n",
        ]
    )
    assert tc.get_clean_log_lines() == [
        "client:Sent message:\n",
        "server:server running in background\n",
    ]


def test_scrub_ipv6lla_port():
    val = scrub("[fe80::abcd:123:12:5432]:12345")
    assert val == "[fe80::xxxx:xxxx:xxxx:xxxx]:xxxxx"


def test_scrub_control():
    assert scrub_control_chars("a\x08b\n") == "ab\n"
